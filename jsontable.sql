CREATE TABLE Joueur (
ID SERIAL PRIMARY KEY,
nom VARCHAR(40) NOT NULL ,
prenom VARCHAR(40) NOT NULL,
surnom VARCHAR(40) NOT NULL,
adresse TEXT NOT NULL,
decks JSON,
UNIQUE(nom, prenom, adresse)
);
CREATE TABLE Organisateur(
ID SERIAL PRIMARY KEY,
nom VARCHAR(40) NOT NULL ,
prenom VARCHAR(40) NOT NULL,
telephone char(10) CHECK (telephone ~ '[[:digit:]]{10}') NOT NULL,
adresse TEXT  NOT NULL,
UNIQUE(nom, prenom, adresse)
);
CREATE TABLE Tournoi(
  annee integer PRIMARY KEY CHECK (annee>1950),
  lieu VARCHAR(40) NOT NULL,
  date_debut DATE NOT NULL,
  joueurNQ JSON,
  Matchs JSON,
  Organisateurs JSON NOT NULL
);

CREATE TABLE Carte(
nom VARCHAR(40) PRIMARY KEY NOT NULL,
extention VARCHAR(40) NOT NULL,
cout INTEGER NOT NULL,
attaque INTEGER,
defense INTEGER,
faculte_speciale VARCHAR(40),
type VARCHAR(40) NOT NULL,
annee_banissement INTEGER,
CHECK ((type ='Ressource' AND cout<0 AND attaque IS NULL AND defense IS NULL) 
OR (cout>=0 AND attaque IS NOT NULL AND defense IS NOT NULL and type='Invention')
OR(cout>=0 AND attaque IS NULL AND defense IS NULL AND faculte_speciale IS NOT NULL AND type='Effets')),
FOREIGN KEY (annee_banissement) REFERENCES Tournoi(annee)
);


INSERT INTO Joueur
(nom, prenom, surnom,adresse,decks)
VALUES 
('Bora', 'Kim', 'YellowStar', '6 rue legendre','[{"id_deck":1,"annee":[2018,2019],"carte":[{"id_carte":"EAU","nombre":15},{"id_carte":"aile de mort","nombre":4},{"id_carte":"contre-sort","nombre":11}]},{"id_deck":5,"annee":[2020],"carte":[{"id_carte":"attaque_direct","nombre":6},{"id_carte":"EAU","nombre":10},{"id_carte":"sejin","nombre":14}]}]'),
('Sang-hyeok', 'Lee', 'Faker', '6 square de corée','[{"id_deck":2,"annee":[2018,2019,2020],"carte":[{"id_carte":"kobald","nombre":5},{"id_carte":"Source","nombre":12},{"id_carte":"attaque_direct","nombre":8},{"id_carte":"guardien_de_la_terre","nombre":5}]}]'),
('Boyer', 'Paul', 'Soaz', '10 rue legendre','[{"id_deck":3,"annee":[2018,2019,2020],"carte":[{"id_carte":"EAU","nombre":18},{"id_carte":"aile de mort","nombre":12}]}]'),
('Murphy-Root', 'Ryan', 'Purple', '9 avenue du marechal','[{"id_deck":4,"annee":[2019,2020],"carte":[{"id_carte":"attaque_direct","nombre":10},{"id_carte":"EAU","nombre":14},{"id_carte":"guardien_de_la_terre","nombre":6}]}]');


INSERT INTO Organisateur
(nom, prenom, telephone,adresse)
VALUES 
('Sylas', 'Lim', '0102030405', '9 rue legendre'),
('Bora', 'Kim','0102454540', '6 rue legendre'),
('Boyers', 'Paul', '0102030406', '10 rue legendre');

INSERT INTO Tournoi
(annee,lieu,date_debut,joueurNQ,Matchs,Organisateurs)
VALUES 
(2018,'Stade de France',to_date('04-21', 'MM-DD'),'[{"id_joueurNQ":3}]','[{"jour_heure":"2-12:00","table_id":1,"resultat":true,"salle":"Salle 1","id_joueur1":1,"id_joueur2":2}]','[{"id_organisateur":1}]'),
(2019,'Stade de France',to_date('05-12', 'MM-DD'),'[{"id_joueurNQ":4}]','[{"jour_heure":"2-12:00","table_id":1,"resultat":true,"salle":"Salle 1","id_joueur1":1,"id_joueur2":3},{"jour_heure":"1-8:00","table_id":2,"resultat":false,"salle":"Salle 1","id_joueur1":1,"id_joueur2":2}]','[{"id_organisateur":1}]'),
(2020,'Stade de Toulouse',to_date('07-11', 'MM-DD'),'[{"id_joueurNQ":4}]','[{"jour_heure":"1-20:00","table_id":2,"resultat":false,"salle":"Salle 1","id_joueur1":2,"id_joueur2":3},{"jour_heure":"1-15:00","table_id":1,"resultat":true,"salle":"Salle 1","id_joueur1":1,"id_joueur2":3}]','[{"id_organisateur":3},{"id_organisateur":2}]');


INSERT INTO Carte
(nom, extention, cout,attaque,defense,faculte_speciale,type,annee_banissement)
VALUES 
('Source','Primera',-2, NULL, NULL, NULL,'Ressource',NULL),
('EAU','Seconda',-1, NULL, NULL, 'rehydrate le personnage','Ressource',NULL),
('aile de mort','Primera',8, 10, 10, NULL,'Invention',2019),
('sejin','Primera',4, 3, 5, NULL,'Invention',NULL),
('guardien_de_la_terre','Primera',3, 6, 2, NULL,'Invention',NULL),
('kobald','Primera',2, 1, 4, '+1 degats des sorts','Invention',NULL),
('contre-sort', 'Primera', 2,NULL,NULL,'contre les sorts','Effets',2020),
('attaque_direct', 'Seconda', 3,NULL,NULL,'inflige 5 degats','Effets',NULL);


/* DEBUT SELECT */
/*  SELECT 1:nombre de joueur n'ayant jamais passé les éliminatoire (mais qui a essayé) */
select COUNT(a.idjnq)as nb_joueur_jamais_Q
from
(
	SELECT jnq->>'id_joueurNQ' as idjnq
	FROM TOURNOI t, JSON_ARRAY_ELEMENTS(t.joueurnq) jnq
	EXCEPT
	(
		SELECT m->>'id_joueur2' as idj2
		FROM TOURNOI t, JSON_ARRAY_ELEMENTS(t.matchs) m
		UNION
		SELECT m->>'id_joueur1' as idj1
		FROM TOURNOI t, JSON_ARRAY_ELEMENTS(t.matchs) m
	)
)a
/* select 2  repartition des types de carte de gagnant de tournoi*/

/* vue intermédiare annee et id_joueur des gagnants de tournoi pour augmenter la lisibilité 2*/
create view ANNE_ID_J_GAGNANT as
select 
	annee as annee,
	id_j as id_j
from
(
	select annee,
		CASE WHEN res='true' THEN idj1
		ELSE idj2
		END AS id_j
	from
	(
		(
		SELECT 
			MAX(to_timestamp(m->>'jour_heure','DD"T"HH24:MI')) as MaxjH,
			t.annee
		FROM TOURNOI t, JSON_ARRAY_ELEMENTS(t.matchs) m
		group by t.annee
		)r1
		Inner join 
		(
		select 
			to_timestamp(m2->>'jour_heure','DD"T"HH24:MI') as JH,
			m2->>'id_joueur1' as idJ1,
			m2->>'id_joueur2' as idJ2,
			m2->>'resultat' as res,
			t.annee as annee2 
		from TOURNOI t,JSON_ARRAY_ELEMENTS(t.matchs) m2
		)r2
		on r1.MaxjH=r2.JH and r1.annee=r2.annee2
	)as r11
)as r21;



/* créer la vue avant le select */

select type,cast(SUM(nb::text::int) as decimal)/(select COUNT(*) from tournoi) as moyenne_carte
from(
	select * 
	from
	(
		select 
			c->>'id_carte' as id_c,
			c->'nombre' as nb,
			d->'id_deck' as id_d
		from 
			JOUEUR j,JSON_ARRAY_ELEMENTS(j.decks) d,JSON_ARRAY_ELEMENTS(d->'carte') c
	)as r1
	inner join CARTE cart
	on cart.nom=r1.id_c
)as r2
inner join 
(
	select id_deck 
	from ANNE_ID_J_GAGNANT
	inner join
	(
		select 
			j.id,
			d->'id_deck' as id_deck,
			a as annee
		from JOUEUR j,JSON_ARRAY_ELEMENTS(j.decks) d,JSON_ARRAY_ELEMENTS(d->'annee') a
	)as d
	on ANNE_ID_J_GAGNANT.annee=d.annee::text::int 
	and d.id=ANNE_ID_J_GAGNANT.id_j::int
)as deck_G
on id_d::text::int=deck_G.id_deck::text::int
group by type




/* SELECT 3: Quel est/sont les joueurs qui ont été qualifié (cad non éliminés dans les préliminaires) dans le plus de tournois ? */
select id,nom,prenom,surnom from
(
	select id as id_joueur_qualif_max from
	(
		select id,COUNT(id)as nbqualif from(
		select DISTINCT(annee,id),annee,id
		from
		(
		SELECT m->>'id_joueur2' as id,t.annee 
		FROM TOURNOI t, JSON_ARRAY_ELEMENTS(t.matchs) m
		UNION ALL
		SELECT m->>'id_joueur1' as id,t.annee  
		FROM TOURNOI t, JSON_ARRAY_ELEMENTS(t.matchs) m
		)d)e
		group by id
	)f
	where nbqualif=
	(
		select MAX(count) from
		(
			select id,COUNT(id) 
			from
			(
				select DISTINCT(annee,id),annee,id
				from
				(
					SELECT m->>'id_joueur2' as id,t.annee 
					FROM TOURNOI t, JSON_ARRAY_ELEMENTS(t.matchs) m
					
					UNION ALL
					
					SELECT m->>'id_joueur1' as id,t.annee  
					FROM TOURNOI t, JSON_ARRAY_ELEMENTS(t.matchs) m
				)a
			)b
			group by id
		)c
	)
)j
inner join JOUEUR
on joueur.id=j.id_joueur_qualif_max::int

/* VIEW */

CREATE VIEW v_match (annee, jour_heure,table_id,resultat,salle,id_joueur1,id_joueur2) AS
SELECT t.annee, m->>'jour_heure' AS jour_heure, m->>'table_id' AS table_id, m->>'resultat' AS resultat , m->>'salle' AS salle , m->>'id_joueur1' AS id_joueur1, m->>'id_joueur2' AS id_joueur2
FROM TOURNOI t, JSON_ARRAY_ELEMENTS(t.matchs) m;

CREATE VIEW v_Non_qualifie (annee, id_joueurNQ) AS
SELECT t.annee, m->>'id_joueurNQ' AS id_joueurNQ
FROM TOURNOI t, JSON_ARRAY_ELEMENTS(t.joueurNQ) m;

CREATE VIEW v_Organise (annee, id_organisateur) AS
SELECT t.annee, m->>'id_organisateur' AS id_deck_NQ
FROM TOURNOI t, JSON_ARRAY_ELEMENTS(t.organisateurs) m;

CREATE VIEW v_Banissement (nom, annee_banissement) AS
SELECT c.nom,c.annee_banissement
FROM Carte c;

CREATE VIEW v_Nb_exemplaire (id_deck,id_carte, nombre) AS
SELECT (m.value -> 'id_deck'::text) AS id_deck, (c.value ->> 'id_carte'::text) AS id_carte, (c.value -> 'nombre'::text) AS nombre 
FROM joueur j, LATERAL json_array_elements(j.decks) m(value), LATERAL json_array_elements((m.value -> 'carte'::text)) c(value);


CREATE VIEW v_Tournoi (annee,lieu, date_debut) AS
SELECT annee,lieu, date_debut
from Tournoi;

CREATE VIEW v_Joueur (id,nom,prenom,surnom,adresse) AS
SELECT id,nom,prenom,surnom,adresse
from Joueur;

CREATE VIEW v_Carte (nom,extention, cout,attaque,defense,faculte_speciale,	type) AS
SELECT nom,extention,cout,attaque,defense,faculte_speciale,type
from Carte;

CREATE VIEW v_Deck (id_joueur,id_deck) AS
SELECT j.id,m->'id_deck'
from Joueur j,JSON_ARRAY_ELEMENTS(j.decks) m;



