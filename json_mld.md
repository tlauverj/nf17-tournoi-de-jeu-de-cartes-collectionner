**Joueur**(#nom:string,#prenom:string,surnom:string,#adresse:string,**Decks:JSON**)
avec { nom, prenom, adresse, surnom NOT NULL;
example:
avec ** Decks:[{"id_deck":,"annee":[...],"carte":[{"id_carte":,"nombre":},..]},..] **  


**Organisateur**(#nom:string,#prenom:string,numero:string,#adresse:string)
avec {numero respectant RegEx des numéros de téléphone; nom, prenom, adresse, tel NOT NULL;
(nom,prenom,adresse) clé candidate}



**Carte**(#nom:string,extention:string,cout:integer,attaque:integer,defense:integer,faculte_speciale:string,type:{'Ressource','Invention','Effets'},annee_banissement=>Tournoi)
avec { nom, extention, cout NOT NULL and ((cout<0 AND attaque NULL AND defense NULL and type='Ressource')  
OR(cout>=0 AND attaque NOT NULL AND defense NOT NULL and type='Invention')  
OR(cout>=0 AND attaque NULL AND defense NULL AND faculte_speciale NOT NULL type='Effets'));  
(nom) clé candidate}  

**Tournoi**(#annee:integer,lieu:string,**Matchs:JSON**,**Decks_Non_qualifie:JSON**,**Organisateurs:JSON**) avec { lieu NOT NULL AND annee>1900}  
example:  
    avec **Matchs=[{"jour_heure":,"table_id":,"result":,"salle":,"id_joueur1":,"id_joueur2":]**  
    avec **joueurNQ[{"id_joueurNQ":},..]**  
    avec **Organisateurs[{"id_organisateur":},..]**  
    







