/* Gestion des droits */
-- Non testé
/* Creation des groupes */

CREATE GROUP Joueurs;
CREATE GROUP Organisateurs;
CREATE GROUP GameDev;

/* Joueurs */
CREATE USER j1 WITH PASSWORD 'j1';
CREATE USER j2 WITH PASSWORD 'j2';

/* Organisateurs */
CREATE USER O1 WITH PASSWORD 'O1';
CREATE USER O2 WITH PASSWORD 'O2';

/* Game Developer */
CREATE USER GD1 WITH PASSWORD 'GD1';


/* Droits aux Joueurs */
GRANT SELECT ON vRessources,vInvention,vEffets,Match_non_qualifie,Match_contre_sois_même,nb_qualifie_tournoi,Deck_comprenant_des_cartes_interdites,Joueur_Organisateur,double_inscription,nombre_carte_error,Qualifie,NbExemplaire,Deck,Banissement,Carte,Match,Tournoi,Organise,Organisateur,Joueur
 TO GROUP Joueurs; 
ALTER GROUP Joueurs ADD USER j1, j2;

/* Droits des Organisateurs */
GRANT SELECT ON vRessources,vInvention,vEffets,Match_non_qualifie,Match_contre_sois_même,nb_qualifie_tournoi,Deck_comprenant_des_cartes_interdites,Joueur_Organisateur,double_inscription,nombre_carte_error,Qualifie,NbExemplaire,Deck,Banissement,Carte,Match,Tournoi,Organise,Organisateur,Joueur TO GROUP Organisateurs; 
GRANT ALL PRIVILEGES ON Joueur,Organisateur,Organise,Tournoi,Match,Banissement TO GROUP Organisateurs;
ALTER GROUP Organisateurs ADD USER O2, O1;

/* Droits des Game Developer  */
GRANT SELECT ON vRessources,vInvention,vEffets,Match_non_qualifie,Match_contre_sois_même,nb_qualifie_tournoi,Deck_comprenant_des_cartes_interdites,Joueur_Organisateur,double_inscription,nombre_carte_error,Qualifie,NbExemplaire,Deck,Banissement,Carte,Match,Tournoi,Organise,Organisateur,Joueur TO GROUP GameDev; 
GRANT ALL PRIVILEGES ON Carte TO GROUP GameDev;
ALTER GROUP GameDev ADD USER GD1;



