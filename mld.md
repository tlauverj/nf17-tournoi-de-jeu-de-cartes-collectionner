MLD
===
# Relations

// heritage par classe fille pour Personne

**Joueur**(#id:int,nom:string,prenom:string,surnom:string,adresse:string)
avec { nom, prenom, adresse, surnom NOT NULL;
 (nom,prenom,adresse) clé candidate}

**Organisateur**(#id:int,nom:string,prenom:string,numero:string,adresse:string)
avec {numero respectant RegEx des numéros de téléphone; nom, prenom, adresse, tel NOT NULL;
 (nom,prenom,adresse) clé candidate}

**Organise**(#id_oranisateur=>Organisateur,#tournoi=>Tournoi)


// heritage par classe mère 
**Carte**(#nom:string,extention:string,cout:integer,attaque:integer,defense:integer,faculte_speciale:string,type:{'Ressource','Invention','Effets'})
avec { nom, extention, cout NOT NULL and ((cout<0 AND attaque NULL AND defense NULL and type='Ressource')
OR(cout>=0 AND attaque NOT NULL AND defense NOT NULL and type='Invention')
OR(cout>=0 AND attaque NULL AND defense NULL AND faculte_speciale NOT NULL type='Effets'));
(nom) clé candidate}


**Tournoi**(#annee:integer,lieu:string,date_debut:date) avec { lieu,date_debut NOT NULL AND annee>1900}


**DECK**(#id_deck:int,id_joueur=>Joueur) 

**NbExemplaire**(#id_carte=>Carte,#id_deck=>Deck,nbexemplaire:integer){avec nbexemplaire NOT NULL and nbexemplaire>=1}

**Nonqualifie**(#id_deck=>Deck,#tournoi=>Tournoi)


**Banissement**(#id_carte=>Carte,#tournoi=>Tournoi){avec tournoi NOT NULL}
//Facilite la recherche de carte bannis


**Match**(#id_match:int,tournoi=>Tournoi,Deck1=>Deck,Deck2=>Deck,jour_heure:timestamp,table:integer,resultat:boolean,salle:string)
avec{jour_heure,table,resultat,salle,tournoi,joueur1,joueur2 NOT NULL ;
avec jour_heure<(3-00:00);  //jour_heure de la forme (numero de la journée - hh:mm)
(jour_heure,table,tournoi) clé candidate, (Deck1,jour_heure,tournoi) clé candidate, (Deck2,jour_heure,tournoi) clé candidate}




# Contraintes:
Héritage exclusif

Intersection(Projection(Joueur, nom, prénom, adresse), Projection(Organisateur, nom, prénom,adresse)) = {}

// un joueur n'a qu'un deck par tournoi
Jointure(Deck,Participe,Deck.id_deck=Participe.id_deck)=DISTINCT(Jointure(Deck,Participe,Deck.id_deck=Participe.id_deck))

// Le deck joué dans le match est qualifié pour le tournoi

// Les cartes du deck ne sont pas encore bannis

# Vues:
vRessource=projection(restriction(Carte,type='Ressource'),nom,extention,Cout,faculte_speciale)
vInvention=projection(restriction(Carte,type='Invention'),nom,extention,Cout,attaque,defense,faculte_speciale)
vEffets=projection(restriction(Carte,type='Effet'),nom,extention,Cout,faculte_speciale)

///


 nb_joueurs_jamais_passé_éliminatoires
 
 répartition moyenne des différents types de carte parmi tous les champions du tournoi
 
 Quel est/sont les joueurs qui ont été qualifié (cad non éliminés dans les préliminaires) dans le plus de tournois ?

# Explication des choix:
**Héritage par classe mère pour Cartes, Inventions, Resosurce, Effet**:
la classe Carte(mère) a plusieurs relations avec d'autres tables et les héritages sont exclusifs, les classes filles n ont pas de relations (autre que celles de la mère) avec d autre tables.

**Héritage par clase fille pour Personne, Joueur, Organisateur**:
La classe mère est abstraite avec aucune relation avec d'autre table.

**Clés artificielles pour Organisateur et Joueur**:
La clé candidate est (nom, prenom, adresse), elle est donc composée de plusieurs éléments ainsi utiliser une clé artificielles nous permet de réduire la clé.

**Clés artificielles pour Match**:
Toutes les clés candidates de Match sont composé de plusieurs éléments

**Redondance des decks dans la table Match**:  
J'ai choisi de créé de la redondance au sein de la table sur l'id du deck pour faciliter la requete 2:"repartition des types de carte de gagnant de tournoi".
J'aurai aussi pu choisir de stocker les id des joueurs participant aux matchs, mais ça aurai impliqué la création d'un lien dans une autre table entre l'id du deck et l'année du tournoi pour lequel il est utilisé, ce qui aurait créé de la redondance entre deux tables (un deck inscrit pour une année et qui n'est pas dans la table Nonqualifie est forcement qualifie) .
