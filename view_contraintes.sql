



-- vérifier qu il n y a pas trop de joueur Qualifie
--ok
CREATE VIEW Nb_joueur_qualifie_tournoi as
select count(*) as nbjoueur,id_tournoi
from(
select id_deck1,id_tournoi
from match
UNION
select id_deck2,id_tournoi
from match)as f
group by id_tournoi;



--ok
CREATE VIEW Deck_NQ_comprenant_des_cartes_interdites as
select NONQUALIFIE.deck,NONQUALIFIE.id_tournoi,nb.id_carte
from NONQUALIFIE
INNER JOIN NBEXEMPLAIRE nb
   ON nb.id_deck = NONQUALIFIE.deck

INNER JOIN BANISSEMENT
  ON BANISSEMENT.id_carte = nb.id_carte

Where BANISSEMENT.id_tournoi_ban<=NONQUALIFIE.id_tournoi;

--ok
CREATE VIEW Deck_Q_comprenant_des_cartes_interdites as
select id_deck,id_carte,id_tournoi
from 
(
select id_deck1,id_tournoi from match
UNION
select id_deck2,id_tournoi from match
)sub1
INNER JOIN
(
select BANISSEMENT.id_carte,BANISSEMENT.id_tournoi_ban,NBEXEMPLAIRE.id_deck from NBEXEMPLAIRE
INNER JOIN BANISSEMENT
ON(BANISSEMENT.id_carte=NBEXEMPLAIRE.id_carte)
)sub4
ON(sub1.id_deck1=sub4.id_deck)
Where sub4.id_tournoi_ban<=sub1.id_tournoi;

-- ok

CREATE VIEW Joueur_Organisateur as
select Joueur.nom,Joueur.prenom,Joueur.adresse
from Joueur 
INNER JOIN Organisateur Org
   ON Org.nom =Joueur.nom and Org.adresse =Joueur.adresse and Org.prenom =Joueur.prenom;
   
 --
 /*
CREATE VIEW double_inscription as
select id_tournoi,id_joueur,Count(*)
from Participe
INNER JOIN Deck
   ON Deck.id_deck =Participe.deck
group by id_tournoi, id_joueur
Having Count(*)>1;
*/
--

--ok
create view nombre_carte_error as
select id_deck 
from deck
EXCEPT(
select  d.id_deck
from NBEXEMPLAIRE
INNER JOIN DECK d
   ON d.id_deck=NBEXEMPLAIRE.id_deck

group by d.id_deck
having SUM(NBEXEMPLAIRE.nombre)=30);



