-- nombre de joueur n'ayant jamais passé les éliminatoire (mais qui a essayé)
SELECT COUNT(*) 
from(
	select DISTINCT(id_joueur) 
	from NONQUALIFIE
	inner join DECK on DECK.id_deck=NONQUALIFIE.deck
	EXCEPT
	(
		select id_joueur  
		from DECK d
		Inner join MATCH m on m.id_deck1=d.id_deck
	)
	EXCEPT
	(
		select id_joueur 
		from DECK d
		Inner join MATCH m on m.id_deck2=d.id_deck
	)
)AS subquery;



-- repartition des types de carte de gagnant de tournoi
select 
	cast(SUM(nombre) as decimal)/(select COUNT(*) from TOURNOI) as nombreMoyenCarte,
	type
from carte ct
INNER JOIN(
	select * from NBEXEMPLAIRE nb
	INNER JOIN
	(
		select 
		CASE 
			WHEN resultat=true THEN id_deck1
			ELSE id_deck2
		END as deckvainqueur
		from match
		INNER JOIN
		(
			select DISTINCT(id_match) from match as m
			INNER JOIN 
			(
				select id_tournoi,MAX(jour_heure) as maxheure from match
				group by id_tournoi
			) as s
			on s.maxheure=m.jour_heure
		)as t
		on match.id_match=t.id_match
	)as n
	on n.deckvainqueur=nb.id_deck
)as p
on p.id_carte=ct.nom
group by type;


--- Quel est/sont les joueurs qui ont été qualifié (cad non éliminés dans les préliminaires) dans le plus de tournois ?

select id_joueur,nom,prenom,surnom
from JOUEUR
inner join
(
	select id_joueur 
	from
	(
		select 
			id_joueur,
			COUNT(id_tournoi)as nbqualif 
		from(
			select 
				DISTINCT(id_joueur,id_tournoi),
				id_joueur,
				id_tournoi 
			from
			(
				select 
					id_joueur,
					id_match,
					id_tournoi 
				from DECK d
				Inner join MATCH m on m.id_deck1=d.id_deck
				UNION
				select 
					id_joueur,
					id_match,
					id_tournoi 
				from DECK d
				Inner join MATCH m on m.id_deck2=d.id_deck
			)f
		)d
		group by id_joueur
	)n
	where nbqualif=
	(
		select 
			MAX(nbqualif) as MAXi 
		from
		(
			select 
				id_joueur,
				COUNT(id_tournoi)as nbqualif 
			from
			(
				select 
						DISTINCT(id_joueur,id_tournoi),
						id_joueur,
						id_tournoi 
				from
				(
					select 
						id_joueur,
						id_match,
						id_tournoi
					from DECK d 
					Inner join MATCH m
					on m.id_deck1=d.id_deck 
					
					UNION 
					
					select 
						id_joueur,
						id_match,
						id_tournoi 
					from DECK d 
					Inner join MATCH m on m.id_deck2=d.id_deck
				)f
			)d 
			group by id_joueur
		)c	
	)
)p
on p.id_joueur=JOUEUR.id;

-- veille version de la requete 1 
SELECT COUNT(*) from(
select id from Joueur
EXCEPT(
select id_joueur from DECK d
Inner join MATCH m
on m.id_deck1=d.id_deck)
EXCEPT(
select id_joueur from DECK d
Inner join MATCH m
on m.id_deck2=d.id_deck)
)AS subquery;
