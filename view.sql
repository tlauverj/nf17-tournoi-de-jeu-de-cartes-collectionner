CREATE VIEW vRessources AS
SELECT c.nom,c.extention, c.Cout,c.faculte_speciale from Carte c
Where type='Ressource';

CREATE VIEW vInvention AS
SELECT c.nom,c.extention, c.Cout,c.attaque,c.defense,c.faculte_speciale from Carte c
Where type='Invention';

CREATE VIEW vEffets AS
SELECT c.nom,c.extention, c.Cout,c.faculte_speciale from Carte c
Where type='Effets';
