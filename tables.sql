CREATE TABLE Joueur(
  ID SERIAL PRIMARY KEY,
  nom VARCHAR(40) NOT NULL,
  prenom VARCHAR(40) NOT NULL,
  surnom VARCHAR(40) NOT NULL,
  adresse VARCHAR(100) NOT NULL,
  UNIQUE(nom,prenom,adresse)
  );
CREATE TABLE Organisateur(
  ID SERIAL PRIMARY KEY,
  nom VARCHAR(40) NOT NULL,
  prenom VARCHAR(40) NOT NULL,
  telephone char(10) CHECK (telephone ~ '[[:digit:]]{10}'),
  adresse VARCHAR(100) NOT NULL,
  UNIQUE(nom,prenom,adresse)
  );
CREATE TABLE Tournoi(
  annee integer PRIMARY KEY CHECK (annee>1950),
  lieu VARCHAR(40)NOT NULL,
  date_debut DATE NOT NULL,
  FOREIGN KEY (annee) REFERENCES Tournoi(annee)
  
);
CREATE TABLE Organise(
  id_organisateur INTEGER,
  id_tournoi INTEGER,
  PRIMARY KEY(id_organisateur,id_tournoi),
  FOREIGN KEY (id_organisateur) REFERENCES Organisateur(ID),
  FOREIGN KEY (id_tournoi) REFERENCES Tournoi(annee)
  
);

CREATE TABLE Carte(
  nom VARCHAR(40) PRIMARY KEY NOT NULL,
  extention VARCHAR(40) NOT NULL,
  cout INTEGER NOT NULL,
  attaque INTEGER,
  defense INTEGER,
  faculte_speciale VARCHAR(40),
  type VARCHAR(40),
  CHECK ((type ='Ressource' AND cout<0 AND attaque IS NULL AND defense IS NULL) OR (cout>=0 AND attaque IS NOT NULL AND defense IS NOT NULL and type='Invention')OR(cout>=0 AND attaque IS NULL AND defense IS NULL AND faculte_speciale IS NOT NULL AND type='Effets'))

);


CREATE TABLE Banissement(
  id_carte VARCHAR(40) PRIMARY KEY NOT NULL,
  id_tournoi_ban INTEGER NOT NULL,
  FOREIGN KEY (id_carte) REFERENCES Carte(nom),
  FOREIGN KEY (id_tournoi_ban) REFERENCES Tournoi(annee)
);

CREATE TABLE Deck(
  id_deck SERIAL NOT NULL,
  id_joueur integer NOT NULL,
  PRIMARY KEY(id_deck),
  FOREIGN KEY (id_joueur) REFERENCES Joueur(ID)
);

CREATE TABLE NbExemplaire(
  id_deck integer NOT NULL,
  id_carte varchar(40) NOT NULL,
  nombre integer NOT NULL,
  PRIMARY KEY(id_carte,id_deck),
  CHECK (nombre>=1) ,
  FOREIGN KEY (id_carte) REFERENCES Carte(nom),
  FOREIGN KEY (id_deck) REFERENCES Deck(id_deck)
);

CREATE TABLE NonQualifie(
  deck integer NOT NULL,
  id_tournoi integer NOT NULL,
  FOREIGN KEY (id_tournoi) REFERENCES Tournoi(annee),
  FOREIGN KEY (deck) REFERENCES Deck(id_deck),
  PRIMARY KEY(id_tournoi,deck)
);
CREATE TABLE Match(
  id_match SERIAL PRIMARY KEY NOT NULL,
  jour_heure timestamp NOT NULL,
  table_id integer NOT NULL,
  resultat boolean NOT NULL,
  Salle VARCHAR(40) NOT NULL,
  id_tournoi integer NOT NULL,
  id_deck1 integer NOT NULL,
  id_deck2 integer NOT NULL,
  CHECK (id_deck1!=id_deck2) ,
  CHECK (jour_heure<'0001-01-03'::timestamp),
  UNIQUE(id_tournoi,jour_heure,table_id,Salle),
  UNIQUE(id_tournoi,jour_heure,id_deck1),
  UNIQUE(id_tournoi,jour_heure,id_deck2),
  FOREIGN KEY (id_tournoi) REFERENCES tournoi(annee),
  FOREIGN KEY (id_deck1) REFERENCES Deck(id_deck),
  FOREIGN KEY (id_deck2) REFERENCES Deck(id_deck)
);
