INSERT INTO Joueur
(nom, prenom, surnom,adresse)
VALUES 
('Bora', 'Kim', 'YellowStar', '6 rue legendre'),
('Sang-hyeok', 'Lee', 'Faker', '6 square de corée'),
('Boyer', 'Paul', 'Soaz', '10 rue legendre'),
('Murphy-Root', 'Ryan', 'Purple', '9 avenue du marechal');

INSERT INTO Organisateur
(nom, prenom, telephone,adresse)
VALUES 
('Sylas', 'Lim', '0102030405', '9 rue legendre'),
('Bora', 'Kim','0102454540', '6 rue legendre'),
('Boyers', 'Paul', '0102030406', '10 rue legendre');

INSERT INTO Carte
(nom, extention, cout,attaque,defense,faculte_speciale,type)
VALUES 
('Source','Primera',-2, NULL, NULL, NULL,'Ressource'),
('EAU','Seconda',-1, NULL, NULL, 'rehydrate le personnage','Ressource'),
('aile de mort','Primera',8, 10, 10, NULL,'Invention'),
('sejin','Primera',4, 3, 5, NULL,'Invention'),
('guardien_de_la_terre','Primera',3, 6, 2, NULL,'Invention'),
('kobald','Primera',2, 1, 4, '+1 degats des sorts','Invention'),
('contre-sort', 'Primera', 2,NULL,NULL,'contre les sorts','Effets'),
('attaque_direct', 'Seconda', 3,NULL,NULL,'inflige 5 degats','Effets');



INSERT INTO Tournoi
(annee,lieu,date_debut)
VALUES 
(2018,'Stade de France',to_date('04-21', 'MM-DD')),
(2019,'Stade de France',to_date('05-12', 'MM-DD')),
(2020,'Stade de Toulouse',to_date('07-11', 'MM-DD'));

INSERT INTO Banissement
(id_carte,id_tournoi_ban)
VALUES
('aile de mort',2019),
('contre-sort',2020);



INSERT INTO Organise
(id_organisateur,id_tournoi)
VALUES 
(1,2018),
(1,2019),
(1,2020),
(2,2020);



INSERT INTO Deck
(id_joueur)
VALUES
(1),
(2),
(3),
(1),
(4);

INSERT INTO NonQualifie
(deck,id_tournoi)
VALUES
(3,2018),
(5,2019),
(5,2020);



INSERT INTO NBEXEMPLAIRE
(id_deck,id_carte,nombre)
VALUES
(1,'EAU',15),
(1,'aile de mort',4),
(1,'contre-sort',11),
(2,'kobald',5),
(2,'Source',12),
(2,'attaque_direct',8),
(2,'guardien_de_la_terre',5),
(3,'EAU',18),
(3,'aile de mort',12),
(4,'attaque_direct',10),
(4,'guardien_de_la_terre',6),
(4,'EAU',14);


INSERT INTO MATCH
(jour_heure,table_id,resultat,salle,id_tournoi,id_deck1,id_deck2)
VALUES
(to_timestamp('1-13','DD-HH24'),1,true,'Salle 1',2018,1,2),
(to_timestamp('1-14','DD-HH24'),1,true,'Salle 1',2019,1,3),
(to_timestamp('2-13','DD-HH24'),2,false,'Salle 1',2019,1,2),
(to_timestamp('1-18','DD-HH24'),2,false,'Salle 1',2020,2,3),
(to_timestamp('2-8','DD-HH24'),1,true,'Salle 1',2020,1,3);
